import React from "react";

function Modal(props) {
  if (!props.winner) {
    return null;
  } else {
    return (
      <div className="modal-style">
        <div>Congrats {props.winner} is winner!!!</div>
      </div>
    );
  }
}

export default Modal;
