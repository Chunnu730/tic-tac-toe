import React from "react";

function Square(props) {
  return (
    <>
      <button className="square" onClick = {props.onClick} style = {props.style}><span>{props.state}</span></button>
    </>
  );
}

export default Square;