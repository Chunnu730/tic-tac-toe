import React from "react";

import Square from "./Square";
function Board(props) {
  return <Square state = {props.state} onClick ={props.onClick} style={props.style}/>;
}
export default Board;