import React, { useState, useEffect } from "react";
import Board from "./components/Board";
import Modal from "./components/Modal";
import "./index.css";
const initialState = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
function App() {
  const [gameState, setUpdateVale] = new useState(initialState);
  const [nextChance, updateNextChance] = new useState(false);
  const onSquareClick = (idx) => {
    let str = Array.from(gameState);
    str[idx] = nextChance ? "X" : "0";
    updateNextChance(!nextChance);
    setUpdateVale(str);
  };

  function checkWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        gameState[a] &&
        gameState[a] === gameState[b] &&
        gameState[a] === gameState[c]
      ) {
        return gameState[a];
      }
    }
    return null;
  }

  const [winner, setWinner] = useState("");
  useEffect(() => {
    const newWinner = checkWinner();
    if (newWinner) {
      setUpdateVale(initialState);
      setWinner(newWinner);
    }
  });
  
  function resetGame() {
    setWinner("");
    setUpdateVale(initialState);
  }

  return (
    <>
      <h2>Tic Toc Toe Game</h2>
      <Modal winner={winner} />
      <div>
        <Board state={gameState[0]} onClick={() => onSquareClick(0)} />
        <Board state={gameState[1]} onClick={() => onSquareClick(1)} />
        <Board state={gameState[2]} onClick={() => onSquareClick(2)} />
      </div>
      <div>
        <Board state={gameState[3]} onClick={() => onSquareClick(3)} />
        <Board state={gameState[4]} onClick={() => onSquareClick(4)} />
        <Board state={gameState[5]} onClick={() => onSquareClick(5)} />
      </div>
      <div>
        <Board state={gameState[6]} onClick={() => onSquareClick(6)} />
        <Board state={gameState[7]} onClick={() => onSquareClick(7)} />
        <Board state={gameState[8]} onClick={() => onSquareClick(8)} />
      </div>
      <div id="btn-box">
        <button onClick={() => resetGame()}>Restart</button>
      </div>
    </>
  );
}

export default App;
